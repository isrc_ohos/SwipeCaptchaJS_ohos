/*
 *    Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
export default {
    props: {
        // 组件参数
        compAttrs: {},
    },
    data: {
        //组件宽度
        simpleWidth: 300,

        //提示文字透明度
        simpleTextOpacity: 1,

        //滑块的动态位置
        simpleBlockPosLeft: 0,
        simpleStartRealXBias: 0, //触摸开始的实际位置
        simpleBlockWidth: 50,

        simpleFunDisabled: false, //使能页面功能

        //时间戳信息
        simpleStartTime: 0,
        simpleEndTime: 0,

        simpleVerifyThreshold: 0.95, // 偏差阈值，进度超过95%通过
    },
    onInit() {
        this.simpleWidth = this.compAttrs.width;
        this.simpleBlockWidth = this.compAttrs.blockSize;
    },
    //开始拖动
    onSimpleSliderStart(e) {
        //记录开始时间和初始触摸位置
        this.simpleStartTime = this.getCurTime();
        this.simpleStartRealXBias = e.touches[0].localX;
    },
    //移动slider
    onSimpleSliderMove(e) {
        let curX = e.touches[0].localX;
        // 确保不越界
        if (curX <= this.simpleStartRealXBias || curX >= this.simpleWidth - this.simpleBlockWidth + this.simpleStartRealXBias) return;
        // 改变滑块位置
        this.simpleBlockPosLeft = curX - this.simpleStartRealXBias;
        // 改变文字透明度
        this.simpleTextOpacity = (1 - 3 * (this.simpleBlockPosLeft / this.simpleWidth)) >= 0 ? (1 - 3 * (this.simpleBlockPosLeft / this.simpleWidth)) : 0;
    },
    //停止拖动
    onSimpleSliderStop(e) {
        //记录结束时间
        this.endTime = this.getCurTime();
        //检查滑块位置
        this.checkSimpleCaptcha(this.simpleBlockPosLeft);
    },

    //检验验证是否成功
    checkSimpleCaptcha(movement) {
        let userXBias = movement;
        let isValid = userXBias >= (this.simpleWidth - this.simpleBlockWidth) * this.simpleVerifyThreshold;

        // 禁用滑块和刷新
        this.simpleFunDisabled = true;
        // 返回结果给父组件
        this.returnRes(isValid);
        // 暂停页面2秒
        setTimeout(() => {
            //重置页面
            this.simpleTextOpacity = 1; //显示提示文本
            this.simpleBlockPosLeft = 0; //滑块归零
            this.simpleFunDisabled = false; //允许使用页面功能
        }, 1000);
    },
    //获取当前时间
    getCurTime() {
        return new Date();
    },
    // 返回验证结果
    returnRes(result) {
        this.$emit("handleResult", {
            result: result
        });
    }
}