# SwipeCaptchaJS_ohos

**本项目是基于鸿蒙滑动验证码项目SwipeCaptcha_ohos的鸿蒙JS API的实现版本，可以通过项目标签以及gitee地址 (https://gitee.com/isrc_ohos/swipe-captcha_ohos2.0)
追踪到原JAVA API版本。**

##### 项目介绍

- 项目名称：滑动验证码

- 所属系列：鸿蒙的第三方组件适配移植

- 编程语言：JavaScript

- 功能：本项目包含三个验证组件的封装，分别为：简易滑动验证 simpleSwipe、滑动拼图验证 swipePuzzle、旋转拼图验证 rotatePuzzle，在前端完成渲染和验证。

- 项目移植状态：100%，已完成全部的功能移植

- 调用差异：重构

- 开发版本：sdk7(兼容sdk4)，DevEco Studio 3.0 Beta2

- 项目发起作者：朱昊东

- 邮箱：[isrc_hm@iscas.ac.cn](mailto:isrc_hm@iscas.ac.cn)

- 原项目Doc地址：[https://gitee.com/isrc_ohos/swipe-captcha_ohos2.0](https://gitee.com/isrc_ohos/swipe-captcha_ohos2.0)

![简易滑动验证 simpleSwipe](https://gitee.com/isrc_ohos/SwipeCaptchaJS_ohos/raw/master/readme_image/1.gif)

<center>简易滑动验证 simpleSwipe</center>

![2](https://gitee.com/isrc_ohos/SwipeCaptchaJS_ohos/raw/master/readme_image/2.gif)

<center>滑动拼图验证 swipePuzzle</center>

![3](https://gitee.com/isrc_ohos/SwipeCaptchaJS_ohos/raw/master/readme_image/3.gif)

<center>旋转拼图验证 rotatePuzzle</center>

##### 使用教程

1. 组件的实现位于 entry/src/main/js/default/common/components/ 中，将组件复制至自己的项目中即可调用这些组件。下面我们以 swipePuzzle 为例演示组件的调用方法，其它组件同理。

2. 在 hml 文件中添加 swipePuzzle并绑定组件所需参数。

   ```html
   <!-- captchaTest.hml -->
   <!-- 指定自定义组件的命名与位置-->
   <element name="swipe" src="../../common/components/swipePuzzle/swipePuzzle.hml">
   </element>
   
   <!--在页面中引入组件-->
   <div class="container" style="background: linear-gradient(#877ad0c6,#909b66a5);"> 
       <swipe img-list="{{swipeImgList}}" comp-attrs="{{swipePuzzleTestAttrs}}" @handle-result="onResReceived">
       </swipe>
   </div>
   ```

   img-list 指定了图片的 url，comp-attrs 指定了组件的参数，自定义事件 handle-result 将验证结果返回给父组件。

3. 在 js 文件中定义组件参数和事件处理函数。主要涉及三个步骤：

    - 指定图片源

      对于滑动拼图验证 swipePuzzle 和旋转拼图验证 rotatePuzzle，我们需要指定图片源数组。此处我们选择本地资源图片的 url，使用 url 字符串组成的数组初始化 img-list。

    - 指定组件参数

      对于不同组件，参数对象具有如下属性：

        - 简易滑动验证 simpleSwipe


          |  参数名   |    解释    |
          | :-------: | :--------: |
          |   width   |  组件宽度  |
          | blockSize | 滑块的宽度 |

        - 滑动拼图验证 swipePuzzle

          | 参数名    | 解释                                       |
          | --------- | ------------------------------------------ |
          | width     | 图片的宽（参数的值需要与图片的宽高比对应） |
          | height    | 图片的高（参数的值需要与图片的宽高比对应） |
          | blockSize | 滑块的大小（会同步更改缺口的大小）         |

        - 旋转拼图验证 rotatePuzzle

          | 参数名 | 解释                                       |
          | ------ | ------------------------------------------ |
          | width  | 图片的宽（参数的值需要与图片的宽高比对应） |
          | height | 图片的高（参数的值需要与图片的宽高比对应） |
          | radius | 旋转区域的半径                             |

    - 指定事件处理函数

      在绑定事件处理函数后，在父组件中可以通过 `e.detail.result` 获取子组件的验证结果。

   js 具体代码如下：

    ```javascript
    // captchaTest.js
    import prompt from '@system.prompt';
    export default {
        data: {
            // 滑动拼图验证的图片列表
            swipeImgList: [
                '/common/images/test1.jpg',
                '/common/images/test2.jpg',
                '/common/images/test3.jpg',
                '/common/images/test4.jpg',
                '/common/images/test5.jpg',
                '/common/images/test6.jpg',
                '/common/images/test7.jpg',
                '/common/images/test8.jpg'
            ],
            // 滑动拼图验证的参数
            swipePuzzleTestAttrs: {
                width: 300,
                height: 225,
                blockSize: 50
            }
        },
        // 事件处理函数，在此处定义验证完成后的操作
        onResReceived(e){
            prompt.showToast({message:"父组件得到结果:"+e.detail.result});
        }
    }
    ```

##### 版本迭代

- v0.1.0-alpha

##### 版权和许可信息

- SwipeCaptchaJS_ohos经过[Apache License, version 2.0](https://gitee.com/link?target=http%3A%2F%2Fwww.apache.org%2Flicenses%2FLICENSE-2.0)授权许可。
