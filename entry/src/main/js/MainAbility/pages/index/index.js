/*
 *    Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC

 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
import prompt from '@system.prompt';

export default {
    data: {
        // 滑动拼图验证的图片列表
        swipeImgList: [
            '/common/images/test1.jpg',
            '/common/images/test2.jpg',
        ],
        // 旋转拼图验证的图片列表
        rotateImgList: [
            '/common/images/test1.jpg',
            '/common/images/test2.jpg',
        ],
        // 简易滑动验证的参数
        simpleSwipeTestAttrs: {
            width: 450,
            blockSize: 75
        },
        // 滑动拼图验证的参数
        swipePuzzleTestAttrs: {
            width: 450,
            height: 337.5,
            blockSize: 75
        },
        // 旋转拼图验证的参数
        rotatePuzzleTestAttrs: {
            width: 450,
            height: 337.5,
            radius: 75
        }
    },
    // 事件处理函数，在此处定义验证完成后的操作
    onResReceived(e) {
        prompt.showToast({
            message: "父组件得到结果:" + e.detail.result
        });
    }
}
